#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
FILE * open_file(char const caminho[]);
char ** file_to_string(FILE * arquivo);

int main(int argc, char const *argv[]) {
	FILE * arquivo = open_file(argv[1]);
	if (arquivo == NULL) {
		return 1;
	}
	char ** str = file_to_string(arquivo);
	//int i;
	//printf("%s\n", str[2]);
	return 0;
}

FILE * open_file(char const caminho[]) {
	FILE *arquivo = fopen(caminho, "r");
	if (arquivo == NULL) {
		printf("Impossivel abrir arquivo\n");
	}
	return arquivo;
}

char ** file_to_string(FILE * arquivo) {
	char buff;
	int i = 0, j = 0;
	char ** vetor;
	while((buff=fgetc(arquivo)) != EOF) {
		if (buff == '\n') i++;
	}
	vetor = malloc(sizeof(char*)*i+1);
	rewind(arquivo);
	i = 0;
	while(true) {
		buff=fgetc(arquivo);
		if (buff == EOF) {
			vetor[i] = (char*)malloc(sizeof(char)*j+1);
			break;
		}
		if (buff == '\n') {
			vetor[i] = (char*)malloc(sizeof(char)*j+1);
			i++,j = 0;
		} else {
			j++;
		}
	}
	rewind(arquivo);
	i = 0, j = 0;
	while(true) {
		buff=fgetc(arquivo);
		if (buff == EOF) {
			vetor[i][j] = '\n';
			vetor[i][j+1] = '\0';
			printf("%s", vetor[i]);
			break;
		} else if (buff == '\n') {
			vetor[i][j] = '\n';
			vetor[i][j+1] = '\0';
			printf("%s", vetor[i]);
			i++,j=0;
		} else {
			vetor[i][j] = buff;
			j++;
		}
	}
	return vetor;
}